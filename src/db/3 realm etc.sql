INSERT INTO job_family
  (id, name, description)
VALUES
  (1, 'Entwickler', 'Entwickler entwickeln Software in den Sprachen Java und Javascript');

  INSERT INTO job_family
  (id, name, description)
VALUES
  (2, 'Datenbank Admin', 'Datenbank-Adminstratoren verwalten und erstellen Datenbanken');

INSERT INTO job_family
  (id, name, description)
VALUES
  (3, 'Marketing', 'Mitarbeiter in der Marketingabteilung sind für die öffentliche Präsentation und Werbung zuständig.');


INSERT INTO realm
  (id, name)
VALUES
  (1, 'intern');

INSERT INTO realm
  (id, name)
VALUES
  (2, 'außendienst');


INSERT INTO status
  (id, name)
VALUES
  (1, 'akzeptiert');

INSERT INTO status
  (id, name)
VALUES
  (2, 'abgelehnt');

INSERT INTO status
  (id, name)
VALUES
  (3, 'in Warteschlange');

INSERT INTO status
  (id, name)
VALUES
  (4, 'akzeptiert aus Warteschlange');

INSERT INTO status
  (id, name)
VALUES
  (5, 'abgelehnt aus Warteschlange');


INSERT INTO role
  (id, name)
VALUES
  (1, 'api');

INSERT INTO role
  (id, name)
VALUES
  (2, 'staff');

INSERT INTO role
  (id, name)
VALUES
  (3, 'superior');

INSERT INTO role
  (id, name)
VALUES
  (4, 'admin');


INSERT INTO language
  (id, name)
VALUES
  (1, 'English');

INSERT INTO language
  (id,name)
VALUES
  (2, 'Deutsch');


INSERT INTO course_type
  (id, name, description)
VALUES
  (1, 'Trainer', 'Ein Kurs mit der von einem Trainer geleitet wird.');

INSERT INTO course_type
  (id, name, description)
VALUES
  (2, 'Online', 'Online-Kurse werden ausschließlich online durchgeführt.');


INSERT INTO competence_level
  (id, name, description)
VALUES
  (1, 'beginner', 'beginner in this field');

INSERT INTO competence_level
  (id, name, description)
VALUES
  (2, 'junior', 'junior');

INSERT INTO competence_level
  (id, name, description)
VALUES
  (3, 'senior', 'senior');

INSERT INTO competence_level
  (id, name, description)
VALUES
  (4, 'expert', 'an expert in this field');