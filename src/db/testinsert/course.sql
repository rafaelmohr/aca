INSERT INTO course
  (id, name, description, active, COURSE_DURATION, COURSE_PREPARATION, MAX_PARTICIPANTS)
VALUES
  (COURSE_S1.nextval, 'Kurs1', 'Beschreibung1', 1, 90, 20, 5);

INSERT INTO course
  (id, name, description, active, COURSE_DURATION, COURSE_PREPARATION, MAX_PARTICIPANTS)
VALUES
  (COURSE_S1.nextval, 'Kurs2', 'Beschreibung2', 0, 567, 2345, 8);

INSERT INTO course
  (id, name, description, active, COURSE_DURATION, COURSE_PREPARATION, MAX_PARTICIPANTS)
VALUES
  (COURSE_S1.nextval, 'Kurs3', 'Beschreibung3', 1, 432, 23, 123);