CREATE OR REPLACE PACKAGE co_grant_internal_pkg
   AUTHID CURRENT_USER
/* 
 * @author gob80 16.01.2014
 *
 * PURPOSE:    utilities to grant rights to other users.
 *             For example to the application-User, online-reporting-User,
 *             or the mm-interface-User.
 */
AS
   /**
      Grants the necessary rights to the application User.
    **/
   PROCEDURE grant_ap_user;

   /**
      Grants the necessary rights to the online-reporting User.
    **/
   PROCEDURE grant_or_user;

   /**
      Grants the execute right to all packages found by naming convention to the MM interface User.
    **/
   PROCEDURE grant_mm_user;

   /**
      Grants the execute right to all packages found by naming convention to the SE interface User.
   **/
   PROCEDURE grant_se_user;

   /**
      Grants the rights to all the named User given in view inet_internal_named_user_right.
   **/
   PROCEDURE grant_named_user;

   /**
      Grants the necessary rights to all users allowed to use the current schema.
      (AP, OR, MM, SE, named-users)
    **/
   PROCEDURE grant_all;
END co_grant_internal_pkg;
/
