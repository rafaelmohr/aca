CREATE OR REPLACE PACKAGE BODY co_grant_internal_pkg
/* 
 * @author gob80 16.01.2014
 *
 * PURPOSE:    utilities to grant rights to other users.
 *             For example to the application-User, online-reporting-User,
 *             or the mm-interface-User.
 *
 * This package needs special views created by the DB admins:
 *    - DB_ROLES (restricted view on SYS.DBA_ROLES)
 *    - DB_ROLE_PRIVS (restricted view on SYS.DBA_ROLE_PRIVS)
 */
AS
   SELECT_ROLE    CONSTANT VARCHAR2 (30) := USER || '_SELECT';
   UPDATE_ROLE    CONSTANT VARCHAR2 (30) := USER || '_UPDATE';
   EXECUTE_ROLE   CONSTANT VARCHAR2 (30) := USER || '_EXECUTE';
   FORMS_ROLE     CONSTANT VARCHAR2 (30) := USER || '_FORMS';

   /* Deprecated: application User (old naming convention) */
   USER_APP       CONSTANT VARCHAR2 (30) := USER || '_APP';
   /* application User */
   USER_AP        CONSTANT VARCHAR2 (30) := USER || '_AP';
   /* online reporting User */
   USER_OR        CONSTANT VARCHAR2 (30) := USER || '_OR';
   /* masterdata interface User */
   USER_MM        CONSTANT VARCHAR2 (30) := USER || '_MM';
   /* SE replication interface User */
   USER_SE        CONSTANT VARCHAR2 (30) := USER || '_SE';

   FUNCTION exists_user(i_username VARCHAR2)
      RETURN BOOLEAN
   AS
      l_result   NUMBER (1);
   BEGIN
      BEGIN
         SELECT 1
           INTO l_result
           FROM all_users
          WHERE username = i_username;

         RETURN TRUE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN FALSE;
      END;
   END exists_user;

   /**
     creates the given role if it does not exist already
    */
   PROCEDURE create_role(i_role VARCHAR2)
   AS
   BEGIN
      FOR l_row IN (SELECT 'create role ' || i_role AS sql_text
                      FROM DUAL
                     WHERE NOT EXISTS
                              (SELECT 1
                                 FROM db_roles
                                WHERE role = i_role))
      LOOP
         dbms_output.put_line(l_row.sql_text);
         EXECUTE IMMEDIATE l_row.sql_text;
      END LOOP;
   END create_role;

   /**
      Assigns the objects to the select role.
    **/
   PROCEDURE assign_select_role_objects
   AS
      l_stmt varchar2(1000);
   BEGIN
      /* create the role if not existing */
      create_role(SELECT_ROLE);

      /* grant the SELECT right for needed objects to the SELECT-role */
      FOR l_row IN (
             SELECT table_name
               FROM user_tables
              WHERE (iot_type <> 'IOT_OVERFLOW' OR iot_type IS NULL)
                AND table_name NOT LIKE '%$%'
                AND table_name NOT LIKE '%INET_INTERNAL%'
             UNION ALL
             SELECT view_name
               FROM user_views
              WHERE view_name NOT LIKE '%$%'
                AND view_name NOT LIKE '%INET_INTERNAL%'
             MINUS
             SELECT table_name
               FROM role_tab_privs
              WHERE role = SELECT_ROLE
                AND owner = user
                AND privilege = 'SELECT'
      )
      LOOP
         l_stmt:= 'grant select on '||l_row.table_name||' to '||SELECT_ROLE;
         dbms_output.put_line(l_stmt);
         EXECUTE IMMEDIATE l_stmt;
      END LOOP;
   END assign_select_role_objects;


   /**
      Assigns the objects to the execute role.
    **/
   PROCEDURE assign_execute_role_objects
   AS
      l_stmt varchar2(1000);
   BEGIN
      /* create the role if not existing */
      create_role(EXECUTE_ROLE);

      /* grant the EXECUTE right for needed objects to the EXECUTE-role */
      FOR l_row IN (
             SELECT DISTINCT object_name
               FROM user_procedures
              WHERE object_name NOT LIKE '%INTERNAL%'
                AND object_name NOT LIKE '%INET_INTERNAL%'
                AND object_type <> 'TRIGGER'
             UNION ALL
             SELECT DISTINCT type_name
               FROM user_types
              WHERE type_name NOT LIKE '%INET_INTERNAL%'
                AND type_name NOT LIKE 'SYS%=='
             MINUS
             SELECT table_name
               FROM role_tab_privs
              WHERE role = EXECUTE_ROLE
                AND owner = user
                AND privilege = 'EXECUTE'
      )
      LOOP
         l_stmt:= 'grant execute on '||l_row.object_name||' to '||EXECUTE_ROLE;
         dbms_output.put_line(l_stmt);
         EXECUTE IMMEDIATE l_stmt;
      END LOOP;

      /* the execute-role also needs the SELECT right on sequences */
      for l_row in (
            SELECT sequence_name
              FROM user_sequences
             WHERE sequence_name NOT LIKE '%INET_INTERNAL%'
            MINUS
            SELECT table_name
              FROM role_tab_privs
             WHERE role = EXECUTE_ROLE
               AND owner = user
               AND privilege = 'SELECT'
      )
      LOOP
         l_stmt:= 'grant select on '||l_row.sequence_name||' to '||EXECUTE_ROLE;
         dbms_output.put_line(l_stmt);
         EXECUTE IMMEDIATE l_stmt;
      END LOOP;
   END assign_execute_role_objects;

   /**
      Assigns the objects to the update role.
    **/
   PROCEDURE assign_update_role_objects
   AS
      l_stmt varchar2(1000);
   BEGIN
      /* create the role if not existing */
      create_role(UPDATE_ROLE);

      /* grant the SELECT, INSERT, UPDATE, DELETE right for needed objects to the UPDATE-role (only SELECT-right for views) */
      FOR l_row in (
             SELECT t.table_name, p.name as priv
               FROM user_tables t
               JOIN table_privilege_map p on p.name in ('SELECT', 'INSERT', 'UPDATE', 'DELETE')
              WHERE     (t.iot_type <> 'IOT_OVERFLOW' OR t.iot_type IS NULL)
                    AND t.table_name NOT LIKE '%$%'
                    AND t.table_name NOT LIKE '%INET_INTERNAL%'
                    AND t.table_name NOT IN (SELECT table_name
                                               FROM user_external_tables)
             UNION ALL
             SELECT v.view_name, 'SELECT' as priv
               FROM user_views v
              WHERE     v.view_name NOT LIKE '%$%'
                    AND v.view_name NOT LIKE '%INET_INTERNAL%'
              MINUS
             select table_name, privilege as priv
               from role_tab_privs
              where role = UPDATE_ROLE
                and owner = user
                and privilege in ('SELECT', 'INSERT', 'UPDATE', 'DELETE')
      )
      loop
          l_stmt:= 'grant '||l_row.priv||' on '||l_row.table_name||' to '||UPDATE_ROLE;
          dbms_output.put_line(l_stmt);
          EXECUTE IMMEDIATE l_stmt;
      end loop;
   END assign_update_role_objects;

   /**
      Assigns the objects to the forms role (for the needed objects in forms and readonly contect).
    **/
   PROCEDURE assign_forms_role_objects
   AS
      l_ncount NUMBER := 0;
      l_stmt varchar2(1000);
   BEGIN
      SELECT COUNT (*)
        INTO l_ncount
        FROM user_procedures
       WHERE object_name LIKE '%JASECURITY%';

      IF l_ncount > 0 THEN
         /* create the role if not existing */
         create_role(FORMS_ROLE);

         /* grant the execute right for needed objects to the forms role */
         FOR l_row IN (
                SELECT DISTINCT object_name
                  FROM user_procedures
                 WHERE object_name LIKE '%JASECURITY%'
                MINUS
                SELECT table_name
                  FROM role_tab_privs
                 WHERE role = FORMS_ROLE
                   AND owner = user
                   AND privilege = 'EXECUTE'
         )
         LOOP
            l_stmt:= 'grant execute on '||l_row.object_name||' to '||FORMS_ROLE;
            dbms_output.put_line(l_stmt);
            EXECUTE IMMEDIATE l_stmt;
         END LOOP;
      END IF;
   END assign_forms_role_objects;

   /**
      (re)creates all the used roles and assigns the objects to the roles
    **/
   PROCEDURE assign_objects_to_roles
   AS
   BEGIN
      assign_select_role_objects;
      assign_execute_role_objects;
      assign_update_role_objects;
      assign_forms_role_objects;
   END assign_objects_to_roles;

   /**
     grants a role
   **/
   PROCEDURE grant_role(i_role VARCHAR2, i_grantee VARCHAR2)
   AS
    l_count number := 0;
    l_stmt varchar2(1000);
   BEGIN
      select count(*)
        into l_count
        from db_role_privs
       where grantee = i_grantee
         and granted_role = i_role;

      if l_count = 0 then
         l_stmt:= 'GRANT ' || i_role || ' to ' || i_grantee;
         dbms_output.put_line(l_stmt);
         EXECUTE IMMEDIATE l_stmt;
      end if;

   END grant_role;

   /**
     grants the privileges needed for AQ.
    */
   PROCEDURE grant_queue_privileges(i_grantee VARCHAR2)
   AS
   BEGIN
      for l_row in (
         SELECT q.name, p.name as privilege
           FROM user_queues q
           join table_privilege_map p on p.name in ('ENQUEUE', 'DEQUEUE')
          WHERE q.queue_type = 'NORMAL_QUEUE'
         MINUS
         SELECT table_name, privilege
           from user_tab_privs
          where grantee = i_grantee
            and type = 'QUEUE'
      )
      loop
         DBMS_AQADM.grant_queue_privilege (l_row.privilege,
                                           l_row.name,
                                           i_grantee,
                                           FALSE);
      end loop;
   END grant_queue_privileges;

   /**
      Grants the necessary rights to the application User.
    **/
   PROCEDURE grant_ap_user
   AS
   BEGIN
      IF exists_user(USER_AP) THEN
         /* creates the roles if not existing and assigns the objects to the roles */
         assign_objects_to_roles;

         /* grant the necessary roles to the application User */
         grant_role(SELECT_ROLE, USER_AP);
         grant_role(UPDATE_ROLE, USER_AP);
         grant_role(EXECUTE_ROLE, USER_AP);

         /* the application User also needs AQ privileges */
         grant_queue_privileges(USER_AP);
      END IF;
   END grant_ap_user;

   /**
      Grants the necessary rights to the online-reporting User.
    **/
   PROCEDURE grant_or_user
   AS
   BEGIN
      IF exists_user(USER_OR) THEN
         /* creates the roles if not existing and assigns the objects to the roles */
         assign_objects_to_roles;

         /* grant the necessary roles to the online-reporting User */
         grant_role(SELECT_ROLE, USER_OR);
         grant_role(EXECUTE_ROLE, USER_OR);
      END IF;
   END grant_or_user;

   /**
      Grants the execute right to all packages found by naming convention to the MM interface User.
    **/
   PROCEDURE grant_mm_user
   AS
   BEGIN
      IF exists_user(USER_MM) THEN
         FOR l_row IN (
                SELECT 'grant execute on '||o.object_name||' to '||u.username AS sql_text
                  FROM all_users u
                      ,user_objects o
                 WHERE u.username = USER_MM
                   AND (   (    o.object_type = 'PACKAGE'
                            AND o.object_name LIKE 'MM\_%' ESCAPE '\')
                        OR (    o.object_type = 'TYPE'
                            AND o.object_name LIKE 'CO\_%' ESCAPE '\'))
                   AND NOT EXISTS (
                               SELECT 'nok'
                                 FROM user_tab_privs p
                                WHERE p.grantee = u.username
                                  AND p.owner = USER
                                  AND p.table_name = o.object_name
                                  AND p.grantor = USER
                                  AND p.privilege = 'EXECUTE')
         )
         LOOP
            dbms_output.put_line(l_row.sql_text);
            EXECUTE IMMEDIATE l_row.sql_text;
         END LOOP;
      END IF;
   END grant_mm_user;

   /**
      Grants the execute right to all packages found by naming convention to the SE interface User.
    **/
   PROCEDURE grant_se_user
   AS
   BEGIN
      IF exists_user(USER_SE) THEN
         FOR l_row IN (
                SELECT 'grant execute on '||o.object_name||' to '||u.username AS sql_text
                  FROM all_users u
                      ,user_objects o
                 WHERE u.username = USER_SE
                   AND o.object_type = 'PACKAGE'
                   AND o.object_name LIKE 'SE\_%' ESCAPE '\'
                   AND NOT EXISTS (
                               SELECT 'nok'
                                 FROM user_tab_privs p
                                WHERE p.grantee = u.username
                                  AND p.owner = USER
                                  AND p.table_name = o.object_name
                                  AND p.grantor = USER
                                  AND p.privilege = 'EXECUTE')
         )
         LOOP
            dbms_output.put_line(l_row.sql_text);
            EXECUTE IMMEDIATE l_row.sql_text;
         END LOOP;
      END IF;
   END grant_se_user;

   /**
      Grants the rights to all the named User given in view inet_internal_named_user_right.
    **/
   PROCEDURE grant_named_user
   AS
      l_count number;
   BEGIN
      SELECT COUNT(*)
        INTO l_count
        FROM user_objects
       WHERE object_name = 'INET_INTERNAL_NAMED_USER';

      IF l_count > 0 THEN
         EXECUTE IMMEDIATE 'begin Inet_Internal_Named_User.Apply_User_Rights; end;';
      END IF;
   END grant_named_user;

   /**
     Grants the necessary rights to all users allowed to use the current schema.
     (AP, OR, MM, SE)
    **/
   PROCEDURE grant_all
   AS
   BEGIN
      grant_ap_user;
      grant_or_user;
      grant_mm_user;
      grant_se_user;
      grant_named_user;
   END grant_all;
END co_grant_internal_pkg;
/
