insert into competence
    (id, name, DESCRIPTION, ASSIGNABLE)
values
     (COMPETENCE_S1.nextval, 'Java', 'java abc', 1);
insert into competence
    (id, name, DESCRIPTION, ASSIGNABLE, PARENT_ID)
values
     (COMPETENCE_S1.nextval, 'Java klassen', 'java abc klassen', 1, 1);
insert into competence
    (id, name, DESCRIPTION, ASSIGNABLE, PARENT_ID)
values
     (COMPETENCE_S1.nextval, 'Java objekte', 'java abc objekte', 1, 1);

insert into competence
    (id, name, DESCRIPTION, ASSIGNABLE)
values
     (COMPETENCE_S1.nextval, 'Marketing', 'Marketing basics', 1);
insert into competence
    (id, name, DESCRIPTION, ASSIGNABLE, PARENT_ID)
values
     (COMPETENCE_S1.nextval, 'Marketing-Strategien', 'Marketing-Strategien erklärt', 1, 2);
insert into competence
    (id, name, DESCRIPTION, ASSIGNABLE, PARENT_ID)
values
     (COMPETENCE_S1.nextval, 'Marketing-Strategien', 'Marketing-Strategien erkllärt', 1, 2);

