INSERT INTO course
  (id, ID_EXTERNAL, NAME, DESCRIPTION, ACTIVE, COURSE_TYPE_ID, LANGUAGE_ID, REALM_ID, COURSE_DURATION, COURSE_PREPARATION, MAX_PARTICIPANTS)
VALUES
  (COURSE_S1.nextval, 'kurs1', 'Kurs 1: Java Einführung', 'In diesem Kurs bekommen sie eine Einführung in Java', 1, 2, 2, 1, 120, 15, 15);
insert into UNIT
    (id, name, COURSE_ID)
values
     (UNIT_S1.nextval, 'Objektorientierung', 1);
insert into CONTENT
    (id, text, UNIT_ID)
values
     (CONTENT_S1.nextval, 'Java Klassen', 1);
insert into CONTENT
    (id, text, UNIT_ID)
values
     (CONTENT_S1.nextval, 'Java Objekte', 1);
insert into UNIT
    (id, name, COURSE_ID)
values
     (UNIT_S1.nextval, 'Java Vererbung', 1);
insert into CONTENT
    (id, text, UNIT_ID)
values
     (CONTENT_S1.nextval, 'Java Abstrakte Klassen', 2);
insert into CONTENT
    (id, text, UNIT_ID)
values
     (CONTENT_S1.nextval, 'Java Interfaces', 2);
insert into CONTENT
    (id, text, UNIT_ID)
values
     (CONTENT_S1.nextval, 'Java Multiple Vererbung', 2);


INSERT INTO course
  (id, ID_EXTERNAL, NAME, DESCRIPTION, ACTIVE, COURSE_TYPE_ID, LANGUAGE_ID, REALM_ID, COURSE_DURATION, COURSE_PREPARATION, MAX_PARTICIPANTS)
VALUES
  (COURSE_S1.nextval, 'kurs2', 'Marketing Methoden', 'In diesem Kurs bekommen sie einen Überblick über die gängigsten Marketing Methoden', 1, 1, 2, 1, 90, 60, 12);
insert into UNIT
    (id, name, COURSE_ID)
values
     (UNIT_S1.nextval, 'Marketing-Geschichte', 2);
insert into CONTENT
    (id, text, UNIT_ID)
values
     (CONTENT_S1.nextval, 'Marketing vor dem Internet', 3);
insert into CONTENT
    (id, text, UNIT_ID)
values
     (CONTENT_S1.nextval, 'Marketing in Zeiten des Internets', 3);