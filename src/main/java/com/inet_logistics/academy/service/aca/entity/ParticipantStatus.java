package com.inet_logistics.academy.service.aca.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@IdClass(ParticipantStatus.class)
public class ParticipantStatus implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "participant_id")
    private Participant participant;

    @Id
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    private Date statusDate;

    private int changedByUserId;

    //region getter & setter

    public Participant getParticipant() {
        return participant;
    }

    public ParticipantStatus setParticipant(Participant participant) {
        this.participant = participant;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public ParticipantStatus setStatus(Status status) {
        this.status = status;
        return this;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public ParticipantStatus setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
        return this;
    }

    public int getChangedByUserId() {
        return changedByUserId;
    }

    public ParticipantStatus setChangedByUserId(int changedByUserId) {
        this.changedByUserId = changedByUserId;
        return this;
    }


    //endregion
}
