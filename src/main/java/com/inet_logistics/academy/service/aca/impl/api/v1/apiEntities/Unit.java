package com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities;


import com.inet_logistics.academy.service.aca.entity.competence.UnitCompetence;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class Unit {
    private String name;

    private ZonedDateTime begin;

    private ZonedDateTime end;

    private List<Content> contents = new ArrayList<>();

    private List<Competence> competences = new ArrayList<>();


    //region getter & setter

    public String getName() {
        return name;
    }

    public Unit setName(String name) {
        this.name = name;
        return this;
    }

    public ZonedDateTime getBegin() {
        return begin;
    }

    public Unit setBegin(ZonedDateTime begin) {
        this.begin = begin;
        return this;
    }

    public ZonedDateTime getEnd() {
        return end;
    }

    public Unit setEnd(ZonedDateTime end) {
        this.end = end;
        return this;
    }

    public List<Content> getContents() {
        return contents;
    }

    public Unit setContents(List<Content> contents) {
        this.contents = contents;
        return this;
    }

    public List<Competence> getCompetences() {
        return competences;
    }

    public Unit setCompetences(List<Competence> competences) {
        this.competences = competences;
        return this;
    }

    //endregion
}
