package com.inet_logistics.academy.service.aca.entity.course;

import org.springframework.data.repository.CrudRepository;

public interface CourseTypeRepository extends CrudRepository<CourseType, Long> {
}
