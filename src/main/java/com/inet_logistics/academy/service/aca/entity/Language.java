package com.inet_logistics.academy.service.aca.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Language {
    @Id
    private Long id;

    @NotNull
    @Column(unique = true)
    @Size(min = 3, max = 255)
    private String name;

    //region getter & setter

    public Long getId() {
        return id;
    }

    public Language setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Language setName(String name) {
        this.name = name;
        return this;
    }

    //endregion
}
