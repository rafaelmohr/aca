package com.inet_logistics.academy.service.aca.impl.api.v1.converter;

import com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CourseConverter {
    private final UnitConverter unitConverter;
    private final PersonConverter personConverter;

    @Autowired
    public CourseConverter(UnitConverter unitConverter, PersonConverter personConverter) {
        this.unitConverter = unitConverter;
        this.personConverter = personConverter;
    }

    @Value("${aca.hostname}")
    private String hostname;

    public Course convert(com.inet_logistics.academy.service.aca.entity.course.Course course) {
        Course courseApi = new Course();

        courseApi
                .setIdExternal(course.getIdExternal())
                .setName(course.getName())
                .setCourseWebLink(hostname + "/course/" + course.getIdExternal())
                .setDescription(course.getDescription())
                .setActive(course.isActive())
                .setRealmName(course.getRealm().getName())
                .setMaxParticipants(course.getMaxParticipants())
                .setCourseDuration(course.getCourseDuration())
                .setCoursePreparation(course.getCoursePreparation())
                .setUnits(unitConverter.convert(course.getUnits()))
                .setCourseType(course.getCourseType())
                .setParticipants(personConverter.convertParticipants(course.getParticipants()))
                .setTrainers(personConverter.convertTrainers(course.getTrainers()));

        return courseApi;
    }

    public List<Course> convert(Iterable<com.inet_logistics.academy.service.aca.entity.course.Course> courses) {
        List<Course> coursesApi = new ArrayList<>();

        for (com.inet_logistics.academy.service.aca.entity.course.Course course : courses) {
            coursesApi.add(convert(course));
        }

        return coursesApi;
    }
}
