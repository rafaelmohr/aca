package com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities;


import com.inet_logistics.academy.service.aca.entity.course.CourseType;

import java.util.ArrayList;
import java.util.List;

public class Course {
    private String idExternal;

    private String name;

    private String courseWebLink;

    private String description;

    private boolean active;

    private String realmName;

    private int maxParticipants;

    private int courseDuration;

    private int coursePreparation;

    private List<Unit> units = new ArrayList<>();

    private CourseType courseType = new CourseType();

    private List<Person> participants = new ArrayList<>();

    private List<Person> trainers = new ArrayList<>();

    //region getter & setter

    public String getIdExternal() {
        return idExternal;
    }

    public Course setIdExternal(String idExternal) {
        this.idExternal = idExternal;
        return this;
    }

    public String getName() {
        return name;
    }

    public Course setName(String name) {
        this.name = name;
        return this;
    }

    public String getCourseWebLink() {
        return courseWebLink;
    }

    public Course setCourseWebLink(String courseWebLink) {
        this.courseWebLink = courseWebLink;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Course setDescription(String description) {
        this.description = description;
        return this;
    }

    public boolean isActive() {
        return active;
    }

    public Course setActive(boolean active) {
        this.active = active;
        return this;
    }

    public String getRealmName() {
        return realmName;
    }

    public Course setRealmName(String realmName) {
        this.realmName = realmName;
        return this;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public Course setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
        return this;
    }

    public int getCourseDuration() {
        return courseDuration;
    }

    public Course setCourseDuration(int courseDuration) {
        this.courseDuration = courseDuration;
        return this;
    }

    public int getCoursePreparation() {
        return coursePreparation;
    }

    public Course setCoursePreparation(int coursePreparation) {
        this.coursePreparation = coursePreparation;
        return this;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public Course setUnits(List<Unit> units) {
        this.units = units;
        return this;
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public Course setCourseType(CourseType courseType) {
        this.courseType = courseType;
        return this;
    }

    public List<Person> getParticipants() {
        return participants;
    }

    public Course setParticipants(List<Person> participants) {
        this.participants = participants;
        return this;
    }

    public List<Person> getTrainers() {
        return trainers;
    }

    public Course setTrainers(List<Person> trainers) {
        this.trainers = trainers;
        return this;
    }


    //endregion


}
