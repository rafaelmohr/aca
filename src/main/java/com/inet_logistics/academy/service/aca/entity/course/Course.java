package com.inet_logistics.academy.service.aca.entity.course;

import com.inet_logistics.academy.service.aca.entity.Language;
import com.inet_logistics.academy.service.aca.entity.Participant;
import com.inet_logistics.academy.service.aca.entity.Realm;
import com.inet_logistics.academy.service.aca.entity.course.unit.Unit;
import com.inet_logistics.academy.service.aca.entity.user.JobFamily;
import com.inet_logistics.academy.service.aca.entity.user.User;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;


/**
 * Base entity for a course.
 *
 * @author rmo80
 */
@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COURSE_S1")
    @SequenceGenerator(sequenceName = "COURSE_S1", allocationSize = 1, name = "COURSE_S1")
    private Long id;

    @NotNull
    @Size(min = 5, max = 256)
    @Column(unique = true)
    private String idExternal;

    @NotNull
    @Size(min = 5, max = 256)
    @Column(unique = true)
    private String name;

    @NotNull
    @Size(min = 5, max = 2023)
    private String description;

    @NotNull
    private boolean active;

    @NotNull
    @DecimalMin("1")
    private int maxParticipants;

    @NotNull
    private int courseDuration;

    @NotNull
    private int coursePreparation;

    @ManyToOne
    @NotNull
    private CourseType courseType;

    @ManyToOne
    //@NotNull
    private Realm realm;

    @ManyToOne
    private Language language;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "course_id")
    private List<Unit> units = new ArrayList<>();

    @OneToMany(
            mappedBy = "course",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Participant> participants = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "course_job_family",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "job_family_id")
    )
    private List<JobFamily> jobFamilies = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "trainer",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id")
    )
    private List<User> trainers = new ArrayList<>();


    //region getter & setter

    public Long getId() {
        return id;
    }

    public Course setId(Long id) {
        this.id = id;
        return this;
    }

    public String getIdExternal() {
        return idExternal;
    }

    public Course setIdExternal(String idExternal) {
        this.idExternal = idExternal;
        return this;
    }

    public String getName() {
        return name;
    }

    public Course setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Course setDescription(String description) {
        this.description = description;
        return this;
    }

    public boolean isActive() {
        return active;
    }

    public Course setActive(boolean active) {
        this.active = active;
        return this;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public Course setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
        return this;
    }

    public int getCourseDuration() {
        return courseDuration;
    }

    public Course setCourseDuration(int courseDuration) {
        this.courseDuration = courseDuration;
        return this;
    }

    public int getCoursePreparation() {
        return coursePreparation;
    }

    public Course setCoursePreparation(int coursePreparation) {
        this.coursePreparation = coursePreparation;
        return this;
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public Course setCourseType(CourseType courseType) {
        this.courseType = courseType;
        return this;
    }

    public Realm getRealm() {
        return realm;
    }

    public Course setRealm(Realm realm) {
        this.realm = realm;
        return this;
    }

    public Language getLanguage() {
        return language;
    }

    public Course setLanguage(Language language) {
        this.language = language;
        return this;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public Course setUnits(List<Unit> units) {
        this.units = units;
        return this;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public Course setParticipants(List<Participant> participants) {
        this.participants = participants;
        return this;
    }

    public List<JobFamily> getJobFamilies() {
        return jobFamilies;
    }

    public Course setJobFamilies(List<JobFamily> jobFamilies) {
        this.jobFamilies = jobFamilies;
        return this;
    }

    public List<User> getTrainers() {
        return trainers;
    }

    public Course setTrainers(List<User> trainers) {
        this.trainers = trainers;
        return this;
    }

    //endregion
}
