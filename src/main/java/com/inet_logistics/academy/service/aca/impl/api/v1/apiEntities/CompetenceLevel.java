package com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities;

public class CompetenceLevel {
    private String name;

    private String description;

    //region getter & setter

    public String getName() {
        return name;
    }

    public CompetenceLevel setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CompetenceLevel setDescription(String description) {
        this.description = description;
        return this;
    }

    //endregion
}
