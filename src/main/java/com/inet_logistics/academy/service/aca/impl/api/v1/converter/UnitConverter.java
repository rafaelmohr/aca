package com.inet_logistics.academy.service.aca.impl.api.v1.converter;

import com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UnitConverter {
    private final ContentConverter contentConverter;
    private final CompetenceConverter competenceConverter;

    @Autowired
    public UnitConverter(ContentConverter contentConverter, CompetenceConverter competenceConverter) {
        this.contentConverter = contentConverter;
        this.competenceConverter = competenceConverter;
    }

    public Unit convert(com.inet_logistics.academy.service.aca.entity.course.unit.Unit unit) {
        Unit unitApi = new Unit();

        unitApi
                .setName(unit.getName())
                .setBegin(unit.getBegin())
                .setEnd(unit.getEnd())
                .setContents(contentConverter.convert(unit.getContents()))
                .setCompetences(competenceConverter.convert(unit.getUnitCompetences()));

        return unitApi;
    }

    public List<Unit> convert(List<com.inet_logistics.academy.service.aca.entity.course.unit.Unit> units) {
        List<Unit> unitsApi = new ArrayList<>();

        for (com.inet_logistics.academy.service.aca.entity.course.unit.Unit unit : units) {
            unitsApi.add(convert(unit));
        }

        return unitsApi;
    }
}
