package com.inet_logistics.academy.service.aca.entity.competence;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CompetenceLevel {
    @Id
    private Long id;

    @NotNull
    @Size(min = 3, max = 32)
    private String name;

    @Size(min = 3, max = 1024)
    private String description;


    //region getter & setter

    public Long getId() {
        return id;
    }

    public CompetenceLevel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CompetenceLevel setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CompetenceLevel setDescription(String description) {
        this.description = description;
        return this;
    }

    //endregion
}
