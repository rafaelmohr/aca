package com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities;

import com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities.CompetenceLevel;

import java.util.List;

public class Competence {
    private String name;

    private String description;

    private CompetenceLevel competenceLevel;

    //private Competence parent;

    //private List<Competence> children;

    private boolean assignable;

    //region getter & setter
    public String getName() {
        return name;
    }

    public Competence setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Competence setDescription(String description) {
        this.description = description;
        return this;
    }

    public boolean isAssignable() {
        return assignable;
    }

    public Competence setAssignable(boolean assignable) {
        this.assignable = assignable;
        return this;
    }

    public CompetenceLevel getCompetenceLevel() {
        return competenceLevel;
    }

    public Competence setCompetenceLevel(CompetenceLevel competenceLevel) {
        this.competenceLevel = competenceLevel;
        return this;
    }

    //endregion
}
