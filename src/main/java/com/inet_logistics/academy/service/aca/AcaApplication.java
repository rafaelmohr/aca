package com.inet_logistics.academy.service.aca;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;

/**
 * Spring boot start class.
 *
 * @author dka80
 */

@SpringBootApplication
public class AcaApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(AcaApplication.class);

    /**
     * Start point for the spring boot application.
     *
     * @param args not used
     */
    public static void main(String[] args) {
        LOGGER.info("Starting Spring Boot application ...");
        SpringApplication.run(AcaApplication.class, args);
        LOGGER.info("Spring Boot application initialized.");
    }

    /**
     * Activates java 8 time zones for springboot / thymeleaf.
     * see <a>http://blog.codeleak.pl/2015/11/how-to-java-8-date-time-with-thymeleaf.html</a>
     *
     * @return Thymeleaf java 8 time dialect
     */
    @Bean
    public Java8TimeDialect java8TimeDialect() {
        return new Java8TimeDialect();
    }

    /**
     * Returns a hello message.
     *
     * @return hello String
     */
    public String sayHello() {
        return "Hello";
    }
}