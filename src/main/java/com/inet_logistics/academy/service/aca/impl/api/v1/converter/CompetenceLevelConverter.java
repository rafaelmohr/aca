package com.inet_logistics.academy.service.aca.impl.api.v1.converter;

import com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities.CompetenceLevel;
import org.springframework.stereotype.Component;

@Component
public class CompetenceLevelConverter {
    public CompetenceLevel convert(com.inet_logistics.academy.service.aca.entity.competence.CompetenceLevel competenceLevel) {
        CompetenceLevel competenceLevelApi = new CompetenceLevel();

        competenceLevelApi
                .setName(competenceLevel.getName())
                .setDescription(competenceLevel.getDescription());

        return competenceLevelApi;
    }
}
