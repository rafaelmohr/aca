package com.inet_logistics.academy.service.aca.entity.course.unit.content;


import com.inet_logistics.academy.service.aca.entity.course.unit.Unit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Base entity for content.
 *
 * @author rmo80
 */
@Entity
public class Content {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTENT_S1")
    @SequenceGenerator(sequenceName = "CONTENT_S1", allocationSize = 1, name = "CONTENT_S1")
    private Long id;

    @NotNull
    @Size(min = 5, max = 4096)
    private String text;


    //region getter & setter

    public Long getId() {
        return id;
    }

    public Content setId(Long id) {
        this.id = id;
        return this;
    }

    public String getText() {
        return text;
    }

    public Content setText(String text) {
        this.text = text;
        return this;
    }

    //endregion
}
