package com.inet_logistics.academy.service.aca.entity.course;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CourseType {
    @Id
    @JsonIgnore
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "course_type_id")
    @JsonIgnore
    private List<Course> courses = new ArrayList<>();

    //region getter & setter

    public Long getId() {
        return id;
    }

    public CourseType setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CourseType setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CourseType setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public CourseType setCourses(List<Course> courses) {
        this.courses = courses;
        return this;
    }

    //endregion
}
