package com.inet_logistics.academy.service.aca.impl.api.v1.converter;

import com.inet_logistics.academy.service.aca.entity.Participant;
import com.inet_logistics.academy.service.aca.entity.user.User;
import com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities.Person;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonConverter {
    public Person convertParticipant(Participant participant) {
        Person personApi = new Person();

        personApi
                .setFirstname(participant.getUser().getFirstname())
                .setLastname(participant.getUser().getLastname())
                .setEmail(participant.getUser().getEmail());

        return personApi;
    }

    public List<Person> convertParticipants(List<Participant> participants) {
        List<Person> participantsApi = new ArrayList<>();

        for (Participant participant : participants) {
            participantsApi.add(convertParticipant(participant));
        }

        return participantsApi;
    }


    public Person convertTrainer(User user) {
        Person personApi = new Person();

        personApi
                .setFirstname(user.getFirstname())
                .setLastname(user.getLastname())
                .setEmail(user.getEmail());

        return personApi;
    }

    public List<Person> convertTrainers(List<User> users) {
        List<Person> personsApi = new ArrayList<>();

        for (User user : users) {
            personsApi.add(convertTrainer(user));
        }

        return personsApi;
    }
}
