package com.inet_logistics.academy.service.aca.impl.api.v1.converter;

import com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities.Content;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ContentConverter {
    public Content convert(com.inet_logistics.academy.service.aca.entity.course.unit.content.Content content) {
        Content contentApi = new Content();

        contentApi.setText(content.getText());

        return contentApi;
    }


    public List<Content> convert(List<com.inet_logistics.academy.service.aca.entity.course.unit.content.Content> contents) {
        List<Content> contentsApi = new ArrayList<>();

        for (com.inet_logistics.academy.service.aca.entity.course.unit.content.Content content : contents) {
            contentsApi.add(convert(content));
        }

        return contentsApi;
    }
}
