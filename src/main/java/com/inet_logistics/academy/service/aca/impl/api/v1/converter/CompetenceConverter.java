package com.inet_logistics.academy.service.aca.impl.api.v1.converter;

import com.inet_logistics.academy.service.aca.entity.competence.UnitCompetence;
import com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities.Competence;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CompetenceConverter {
    private final CompetenceLevelConverter competenceLevelConverter;

    public CompetenceConverter(CompetenceLevelConverter competenceLevelConverter) {
        this.competenceLevelConverter = competenceLevelConverter;
    }

    public Competence convert(UnitCompetence unitCompetence) {
        Competence competenceApi = new Competence();

        competenceApi
                .setName(unitCompetence.getCompetence().getName())
                .setDescription(unitCompetence.getCompetence().getDescription())
                .setAssignable(unitCompetence.getCompetence().isAssignable())
                .setCompetenceLevel(competenceLevelConverter.convert(unitCompetence.getCompetenceLevel()));

        return competenceApi;
    }


    public List<Competence> convert(List<UnitCompetence> unitCompetences) {
        List<Competence> competencesApi = new ArrayList<>();

        for (UnitCompetence unitCompetence : unitCompetences) {
            competencesApi.add(convert(unitCompetence));
        }

        return competencesApi;
    }
}
