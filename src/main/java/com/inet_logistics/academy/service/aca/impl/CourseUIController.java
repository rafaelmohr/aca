package com.inet_logistics.academy.service.aca.impl;

import com.inet_logistics.academy.service.aca.entity.LanguageRepository;
import com.inet_logistics.academy.service.aca.entity.course.Course;
import com.inet_logistics.academy.service.aca.entity.course.CourseRepository;
import com.inet_logistics.academy.service.aca.entity.course.CourseTypeRepository;
import com.inet_logistics.academy.service.aca.entity.user.JobFamilyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;

/**
 * Controller for ui request & response course actions.
 *
 * @author dka80
 */
@Controller
@RequestMapping("/course")
public class CourseUIController {

    private final CourseRepository courseRepository;
    private final CourseTypeRepository courseTypeRepository;
    private final LanguageRepository languageRepository;
    private final JobFamilyRepository jobFamilyRepository;

    @Autowired
    public CourseUIController(CourseRepository courseRepository, CourseTypeRepository courseTypeRepository, LanguageRepository languageRepository, JobFamilyRepository jobFamilyRepository) {
        this.courseRepository = courseRepository;
        this.courseTypeRepository = courseTypeRepository;
        this.languageRepository = languageRepository;
        this.jobFamilyRepository = jobFamilyRepository;
    }

    /**
     * List all courses.
     *
     * @param model View course
     * @return Thymeleaf view instance
     */
    @GetMapping(value = "")
    public String list(Model model, @RequestParam(required = false) String search) {
        model.addAttribute("courses", courseRepository.findByIdExternalContainsIgnoreCaseOrNameContainsIgnoreCase(search, search));
        return "course/courseList";
    }

    @RequestMapping(path="/{idExternal}")
    public String detail(Model model, @PathVariable("idExternal") String idExternal) {
        model.addAttribute("course", courseRepository.findCourseByIdExternal(idExternal));
        return "course/courseDetail";
    }

    /**
     * Adds a new Course.
     *
     * @param model View course
     * @return Thymeleaf view instance
     */
    @GetMapping(value = "/new")
    public String newProject(Model model) {
        model.addAttribute("course", new Course());
        model.addAttribute("courseTypes", courseTypeRepository.findAll());
        model.addAttribute("languages", languageRepository.findAll());
        model.addAttribute("jobFamilies", jobFamilyRepository.findAll());
        return "course/courseNew";
    }

    /**
     * Edits a course.
     *
     * @param model         View course
     * @param course        Course instance with new filled in data
     * @param bindingResult Check results
     * @return Thymeleaf view instance
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String update(Model model, @Valid Course course, BindingResult bindingResult) throws IOException {
        /*if (bindingResult.hasErrors()) {
            return "course/courseEdit";
        }*/

        courseRepository.save(course);

        return "redirect:/course/" + course.getIdExternal();
    }


    //TODO: !!! implement id workaround!!!
    @RequestMapping(value = "/{idExternal}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String idExternal, Model model) {
        Course course = courseRepository.findCourseByIdExternal(idExternal).get();
        model.addAttribute("course", course);
        model.addAttribute("courseTypes", courseTypeRepository.findAll());
        model.addAttribute("languages", languageRepository.findAll());
        model.addAttribute("jobFamilies", jobFamilyRepository.findAll());
        return "course/courseEdit";
    }


    /**
     * Deletes a course with an pk.
     *
     * @param id course pk
     * @return Thymeleaf view instance
     */
    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable long id) {
        courseRepository.delete(courseRepository.findById(id).get());
        return new ModelAndView("redirect:/course?search");
    }
}
