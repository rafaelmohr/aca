package com.inet_logistics.academy.service.aca.entity.user;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inet_logistics.academy.service.aca.entity.Realm;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;


/**
 * Base entity for a role.
 *
 * @author rmo80
 */
@Entity
public class Role {
    @Id
    private Long id;

    @NotNull
    private String name;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_realm",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "realm_id")
    )
    @JsonIgnore
    private List<Realm> realms = new ArrayList<>();





    //region getter & setter

    public Long getId() {
        return id;
    }

    public Role setId(Long id) {
        this.id = id;
        return this;
    }

    public List<Realm> getRealms() {
        return realms;
    }

    public Role setRealms(List<Realm> realms) {
        this.realms = realms;
        return this;
    }

    //endregion
}
