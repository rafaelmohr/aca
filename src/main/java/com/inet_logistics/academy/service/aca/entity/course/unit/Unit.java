package com.inet_logistics.academy.service.aca.entity.course.unit;


import com.inet_logistics.academy.service.aca.entity.competence.UnitCompetence;
import com.inet_logistics.academy.service.aca.entity.course.Course;
import com.inet_logistics.academy.service.aca.entity.course.unit.content.Content;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Base entity for a unit.
 *
 * @author rmo80
 */
@Entity
public class Unit {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UNIT_S1")
    @SequenceGenerator(sequenceName = "UNIT_S1", allocationSize = 1, name = "UNIT_S1")
    private Long id;

    @NotNull
    @Size(min = 5, max = 256)
    @Column(unique = true)
    private String name;


    //@NotNull
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime begin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private transient LocalDate begindate;
    @DateTimeFormat(pattern = "HH:mm")
    private transient LocalTime begintime;


    //@NotNull
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime end;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private transient LocalDate enddate;
    @DateTimeFormat(pattern = "HH:mm")
    private transient LocalTime endtime;


    @ManyToOne
    @NotNull
    private Course course;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "unit_id")
    private List<Content> contents = new ArrayList<>();

    @OneToMany(
            mappedBy = "unit",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<UnitCompetence> unitCompetences = new ArrayList<>();


    //region getter & setter

    public Long getId() {
        return id;
    }

    public Unit setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Unit setName(String name) {
        this.name = name;
        return this;
    }

    public ZonedDateTime getBegin() {
        return begin;
    }

    public Unit setBegin(ZonedDateTime begin) {
        this.begin = begin;
        return this;
    }

    public LocalDate getBegindate() {
        return begindate;
    }

    public Unit setBegindate(LocalDate begindate) {
        this.begindate = begindate;
        return this;
    }

    public LocalTime getBegintime() {
        return begintime;
    }

    public Unit setBegintime(LocalTime begintime) {
        this.begintime = begintime;
        return this;
    }

    public ZonedDateTime getEnd() {
        return end;
    }

    public Unit setEnd(ZonedDateTime end) {
        this.end = end;
        return this;
    }

    public LocalDate getEnddate() {
        return enddate;
    }

    public Unit setEnddate(LocalDate enddate) {
        this.enddate = enddate;
        return this;
    }

    public LocalTime getEndtime() {
        return endtime;
    }

    public Unit setEndtime(LocalTime endtime) {
        this.endtime = endtime;
        return this;
    }

    public Course getCourse() {
        return course;
    }

    public Unit setCourse(Course course) {
        this.course = course;
        return this;
    }

    public List<Content> getContents() {
        return contents;
    }

    public Unit setContents(List<Content> contents) {
        this.contents = contents;
        return this;
    }

    public List<UnitCompetence> getUnitCompetences() {
        return unitCompetences;
    }

    public Unit setUnitCompetences(List<UnitCompetence> unitCompetences) {
        this.unitCompetences = unitCompetences;
        return this;
    }


    //endregion
}