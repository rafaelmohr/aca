package com.inet_logistics.academy.service.aca.entity.user;

import org.springframework.data.repository.CrudRepository;

public interface JobFamilyRepository extends CrudRepository<JobFamily, Long> {
}
