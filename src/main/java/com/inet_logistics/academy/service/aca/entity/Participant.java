package com.inet_logistics.academy.service.aca.entity;


import com.inet_logistics.academy.service.aca.entity.course.Course;
import com.inet_logistics.academy.service.aca.entity.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Base entity for Person.
 *
 * @author rmo80
 */
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"course_id", "user_id"})
})
public class Participant implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARTICIPANT_S1")
    @SequenceGenerator(sequenceName = "PARTICIPANT_S1", allocationSize = 1, name = "PARTICIPANT_S1")
    private Long id;


    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private boolean passed = false;

    private int grade_level;

    private String certMimetype;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] cert;

    private int lastStatusIdR;

    @OneToMany(
            mappedBy = "participant",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<ParticipantStatus> participantStatuses = new ArrayList<>();


    //region getter & setter

    public Long getId() {
        return id;
    }

    public Participant setId(Long id) {
        this.id = id;
        return this;
    }

    public Course getCourse() {
        return course;
    }

    public Participant setCourse(Course course) {
        this.course = course;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Participant setUser(User user) {
        this.user = user;
        return this;
    }

    public boolean isPassed() {
        return passed;
    }

    public Participant setPassed(boolean passed) {
        this.passed = passed;
        return this;
    }

    public int getGrade_level() {
        return grade_level;
    }

    public Participant setGrade_level(int grade_level) {
        this.grade_level = grade_level;
        return this;
    }

    public String getCertMimetype() {
        return certMimetype;
    }

    public Participant setCertMimetype(String certMimetype) {
        this.certMimetype = certMimetype;
        return this;
    }

    public int getLastStatusIdR() {
        return lastStatusIdR;
    }

    public Participant setLastStatusIdR(int lastStatusIdR) {
        this.lastStatusIdR = lastStatusIdR;
        return this;
    }

    public List<ParticipantStatus> getParticipantStatuses() {
        return participantStatuses;
    }

    public Participant setParticipantStatuses(List<ParticipantStatus> participantStatuses) {
        this.participantStatuses = participantStatuses;
        return this;
    }


    //endregion
}
