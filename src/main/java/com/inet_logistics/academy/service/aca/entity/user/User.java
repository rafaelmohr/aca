package com.inet_logistics.academy.service.aca.entity.user;


import com.inet_logistics.academy.service.aca.entity.Participant;
import com.inet_logistics.academy.service.aca.entity.competence.UserCompetence;
import com.inet_logistics.academy.service.aca.entity.course.Course;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;


/**
 * Base entity for a user.
 *
 * @author rmo80
 */
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_S1")
    @SequenceGenerator(sequenceName = "USER_S1", allocationSize = 1, name = "USER_S1")
    private Long id;

    @NotNull
    @Size(min = 3, max = 32)
    @Column(unique = true)
    private String username;

    @Size(min = 6, max = 64)
    private String password;

    @NotNull
    @Size(min = 2, max = 255)
    private String firstname;

    @NotNull
    @Size(min = 2, max = 255)
    private String lastname;

    @NotNull
    @Size(min = 4, max = 255)
    private String email;

    private String imageMimeType;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] image;

    @ManyToMany
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private List<Role> roles = new ArrayList<>();


    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Participant> courses = new ArrayList<>();

    @ManyToOne
    @NotNull
    private JobFamily jobFamily;

    @ManyToMany(mappedBy = "trainers")
    private List<Course> trainerCourses = new ArrayList<>();

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<UserCompetence> userCompetences = new ArrayList<>();


    //region getter & setter

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public User setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public User setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getImageMimeType() {
        return imageMimeType;
    }

    public User setImageMimeType(String imageMimeType) {
        this.imageMimeType = imageMimeType;
        return this;
    }

    public byte[] getImage() {
        return image;
    }

    public User setImage(byte[] image) {
        this.image = image;
        return this;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public User setRoles(List<Role> roles) {
        this.roles = roles;
        return this;
    }

    public List<Participant> getCourses() {
        return courses;
    }

    public User setCourses(List<Participant> courses) {
        this.courses = courses;
        return this;
    }

    public JobFamily getJobFamily() {
        return jobFamily;
    }

    public User setJobFamily(JobFamily jobFamily) {
        this.jobFamily = jobFamily;
        return this;
    }

    public List<Course> getTrainerCourses() {
        return trainerCourses;
    }

    public User setTrainerCourses(List<Course> trainerCourses) {
        this.trainerCourses = trainerCourses;
        return this;
    }

    public List<UserCompetence> getUserCompetences() {
        return userCompetences;
    }

    public User setUserCompetences(List<UserCompetence> userCompetences) {
        this.userCompetences = userCompetences;
        return this;
    }

    //endregion
}
