package com.inet_logistics.academy.service.aca.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Status {
    @Id
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;


    //region getter & setter

    public Long getId() {
        return id;
    }

    public Status setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Status setName(String name) {
        this.name = name;
        return this;
    }


    //endregion
}
