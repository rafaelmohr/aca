package com.inet_logistics.academy.service.aca.entity.competence;

import com.inet_logistics.academy.service.aca.entity.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@IdClass(UserCompetence.class)
public class UserCompetence implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Id
    @ManyToOne
    @JoinColumn(name = "competence_id")
    private Competence competence;

    @ManyToOne
    @NotNull
    private CompetenceLevel competenceLevel;

    //region getter & setter

    public User getUser() {
        return user;
    }

    public UserCompetence setUser(User user) {
        this.user = user;
        return this;
    }

    public Competence getCompetence() {
        return competence;
    }

    public UserCompetence setCompetence(Competence competence) {
        this.competence = competence;
        return this;
    }

    public CompetenceLevel getCompetenceLevel() {
        return competenceLevel;
    }

    public UserCompetence setCompetenceLevel(CompetenceLevel competenceLevel) {
        this.competenceLevel = competenceLevel;
        return this;
    }

    //endregion
}
