package com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities;

public class Person {
    private String firstname;

    private String lastname;

    private String email;


    //region getter & setter

    public String getFirstname() {
        return firstname;
    }

    public Person setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public Person setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Person setEmail(String email) {
        this.email = email;
        return this;
    }

    //endregion
}
