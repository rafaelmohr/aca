package com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities;

public class Content {
    private String text;


    public String getText() {
        return text;
    }

    public Content setText(String text) {
        this.text = text;
        return this;
    }
}
