package com.inet_logistics.academy.service.aca.impl.api.v1;

import com.inet_logistics.academy.service.aca.entity.course.CourseRepository;
import com.inet_logistics.academy.service.aca.impl.api.v1.apiEntities.Course;
import com.inet_logistics.academy.service.aca.impl.api.v1.converter.CourseConverter;
import com.inet_logistics.academy.service.aca.impl.api.v1.exception.BadRequestException;
import com.inet_logistics.academy.service.aca.impl.api.v1.exception.ForbiddenException;
import com.inet_logistics.academy.service.aca.impl.api.v1.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * REST controller for courses.
 *
 * @author rafael mohr
 */
@RestController
@RequestMapping("/api/v1/course")
public class CourseRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseRestController.class);


    private CourseRepository courseRepository;

    private CourseConverter courseConverter;

    @Autowired
    public CourseRestController(CourseRepository courseRepository, CourseConverter courseConverter) {
        this.courseRepository = courseRepository;
        this.courseConverter = courseConverter;
    }


    /**
     * List all courses in the DB
     *
     * @return List with all courses (as JSON)
     */
    @GetMapping("findAll")
    public Iterable<Course> findAll() {
        LOGGER.info("findAll Method called...");
        return courseConverter.convert(courseRepository.findAll());
        //TODO if arraylist is empty
    }

    @GetMapping("findAllActive")
    public Iterable<Course> findAllActive() {
        LOGGER.info("findAllActive Method called...");
        return courseConverter.convert(courseRepository.findByActiveIsTrue());
        //TODO if arraylist is empty
    }


    @GetMapping("find")
    public Course findByIdExternal(@RequestParam String id) {
        Optional<com.inet_logistics.academy.service.aca.entity.course.Course> dbResult = courseRepository.findCourseByIdExternal(id);

        if (dbResult.isPresent()) return courseConverter.convert(dbResult.get());
        else return null;
        //TODO else if optional is empty

    }
}
