package com.inet_logistics.academy.service.aca.entity;

import com.inet_logistics.academy.service.aca.entity.course.Course;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Realm {
    @Id
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "realm_id")
    private List<Course> courses = new ArrayList<>();


    //region getter & setter

    public Long getId() {
        return id;
    }

    public Realm setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Realm setName(String name) {
        this.name = name;
        return this;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public Realm setCourses(List<Course> courses) {
        this.courses = courses;
        return this;
    }

    //endregion
}
