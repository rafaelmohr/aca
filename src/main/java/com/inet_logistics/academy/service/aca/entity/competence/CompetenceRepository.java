package com.inet_logistics.academy.service.aca.entity.competence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetenceRepository extends CrudRepository<Competence, Long> {
}