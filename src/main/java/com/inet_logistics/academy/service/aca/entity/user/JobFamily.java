package com.inet_logistics.academy.service.aca.entity.user;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class JobFamily {
    @Id
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id")
    private List<User> users = new ArrayList<>();


    //region getter & setter

    public Long getId() {
        return id;
    }

    public JobFamily setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public JobFamily setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public JobFamily setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<User> getUsers() {
        return users;
    }

    public JobFamily setUsers(List<User> users) {
        this.users = users;
        return this;
    }


    //endregion
}
