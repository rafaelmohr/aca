package com.inet_logistics.academy.service.aca.entity.competence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inet_logistics.academy.service.aca.entity.course.unit.Unit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@IdClass(UnitCompetence.class)
public class UnitCompetence implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "unit_id")
    @JsonIgnore
    private Unit unit;

    @Id
    @ManyToOne
    @JoinColumn(name = "competence_id")
    private Competence competence;

    @ManyToOne
    @NotNull
    private CompetenceLevel competenceLevel;

    //region getter & setter

    public Unit getUnit() {
        return unit;
    }

    public UnitCompetence setUnit(Unit unit) {
        this.unit = unit;
        return this;
    }

    public Competence getCompetence() {
        return competence;
    }

    public UnitCompetence setCompetence(Competence competence) {
        this.competence = competence;
        return this;
    }

    public CompetenceLevel getCompetenceLevel() {
        return competenceLevel;
    }

    public UnitCompetence setCompetenceLevel(CompetenceLevel competenceLevel) {
        this.competenceLevel = competenceLevel;
        return this;
    }

    //endregion
}
