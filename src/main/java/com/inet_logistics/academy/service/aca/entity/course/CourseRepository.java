package com.inet_logistics.academy.service.aca.entity.course;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

/**
 * SpringBoot default CRUD actions.
 *
 * @author dka80
 */
@Repository
public interface CourseRepository extends CrudRepository<Course, Long> {

    Optional<Course> findCourseByIdExternal(String id);

    ArrayList<Course> findByActiveIsTrue();

    ArrayList<Course> findByIdExternalContainsIgnoreCaseOrNameContainsIgnoreCase(String searchStringIdExternal, String searchStringName);
}
