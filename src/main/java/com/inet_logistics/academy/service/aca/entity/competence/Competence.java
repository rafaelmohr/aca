package com.inet_logistics.academy.service.aca.entity.competence;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Competence {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMPETENCE_S1")
    @SequenceGenerator(sequenceName = "COMPETENCE_S1", allocationSize = 1, name = "COMPETENCE_S1")
    private Long id;

    @NotNull
    @Size(min = 3, max = 255)
    private String name;

    @Size(min = 3, max = 1024)
    private String description;

    @ManyToOne
    private Competence parent;

    @OneToMany(mappedBy = "parent")
    private List<Competence> children;

    @NotNull
    private boolean assignable;

    @OneToMany(
            mappedBy = "competence",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<UserCompetence> userCompetences = new ArrayList<>();

    @OneToMany(
            mappedBy = "competence",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<UnitCompetence> unitCompetences = new ArrayList<>();


    //region getter & setter

    public Long getId() {
        return id;
    }

    public Competence setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Competence setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Competence setDescription(String description) {
        this.description = description;
        return this;
    }

    public Competence getParent() {
        return parent;
    }

    public Competence setParent(Competence parent) {
        this.parent = parent;
        return this;
    }

    public List<Competence> getChildren() {
        return children;
    }

    public Competence setChildren(List<Competence> children) {
        this.children = children;
        return this;
    }

    public boolean isAssignable() {
        return assignable;
    }

    public Competence setAssignable(boolean assignable) {
        this.assignable = assignable;
        return this;
    }

    public List<UserCompetence> getUserCompetences() {
        return userCompetences;
    }

    public Competence setUserCompetences(List<UserCompetence> userCompetences) {
        this.userCompetences = userCompetences;
        return this;
    }

    public List<UnitCompetence> getUnitCompetences() {
        return unitCompetences;
    }

    public Competence setUnitCompetences(List<UnitCompetence> unitCompetences) {
        this.unitCompetences = unitCompetences;
        return this;
    }

    //endregion
}
